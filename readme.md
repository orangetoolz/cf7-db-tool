# CF7 DB Tool

##### Requires at least: Wordpress 4.8
##### Tested up to: Wordpress 5.0.3
##### Stable Tag: 2.0.0
##### License: GPLv2 or later
##### License URI: https://www.gnu.org/licenses/gpl-2.0.html

#### Description
Save all Wordpress Contact form 7 Submission data inside wp-admin. Very Easy plug and play installation and too easy to use. After installing the plugin it automatically captures and stores all contact form 7 submission inside the wp-admin interface.

#### Some major Features are listed below:

* Plug and Play addon.
* No configuration required.
* Store all data submitted by contact form 7 and show the list inside the wp-admin.
* Easy lightweight plugin.
* You can Search data.
* Very user-friendly pagination.
* Select Any contact form and view the data of that specific form.
* Export selected form entries in csv
* Export all entries in csv.
* Send individual reply email form admin (Available if form submission contains any email address)

#### Upcoming features:

* Send bulk reply email form admin


#### Installation

1. Download and extract plugin files to a wp-content/plugin directory.
2. Activate the plugin through the WordPress admin interface and you are all set.
3. After activate the plugin there will be a option in WordPress admin menu named `CF7 DB Tool` where you will find plugin page

#### Screenshots
1. Contact forms list. Click on form to see its entries
![Screenshot-1](/assets/screenshot-1.png "Contact forms list. Click on form to see its entries")<br>
2. List of form submissions in this contact form
![Screenshot-2](/assets/screenshot-2.png "List of form submissions in this contact form")<br>
3. Export selected form table
![Screenshot-3](/assets/screenshot-3.png "Export selected form table")
4. Export all entry under a form
![Screenshot-4](/assets/screenshot-4.png "Export all entry under a form ")
5. Details view of a submission
![Screenshot-5](/assets/screenshot-5.png "Details view of a submission")
6. Reply option
![Screenshot-6](/assets/screenshot-6.png "Reply option")

#### Frequently asked questions

>How can I export all entries under a specific form?
>>If you don't select any entry from table and select option `Export to csv` and click apply button, you will get a csv file that contains all entries of the form.

>Why I don't have a reply by email option?
>>`Reply by email` option only available if your form have a email field.

#### Changelog

**1.0.0:** *Release Date - January 09, 2019*

* Initial release of plugin.

**1.1.0:** *Release Date - February 09, 2019*

* Added CSV export feature.

**2.0.0:** *Release Date - February 10, 2019*

* Added email reply feature.