=== CF7 DB Tool ===
Contributors: rahat89
Tags: contact, cf7, contact form 7, contact form 7 db, contact form db, contact form seven, contact form storage, export contact form, save contact form, wpcf7
Requires at least: 4.8
Tested up to: 5.0.3
Stable tag: 1.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Save all Wordpress Contact form 7 Submission data inside wp-admin. Very Easy plug and play installation and too easy to use.

== Description ==

After installing the plugin it automatically captures and stores all contact form 7 submission inside the wp-admin interface.

Some major Features are listed below:

* Plug and Play addon.
* No configuration required.
* Store all data submitted by contact form 7 and show the list inside the wp-admin.
* Easy lightweight plugin.
* You can Search data.
* Very user-friendly pagination.
* Select Any contact form and view the data of that specific form.
* Export selected form entries in csv
* Export all entries in csv.
* Send individual reply email form admin (Available if form submission contains any email address)

Upcoming features:

* Send bulk reply email form admin


== Installation ==

1. Download and extract plugin files to a wp-content/plugin directory.
2. Activate the plugin through the WordPress admin interface and you are all set.
3. After activate the plugin there will be a option in WordPress admin menu named `CF7 DB Tools` where you will find plugin page\

== Screenshots ==

1. Contact forms list. Click on form to see its entries
2. List of form submissions in this contact form
3. Export selected form table
4. Export all entry under a form (If you don't select any entry)
5. Details view of a submission
6. Reply option

== Frequently asked questions ==

= How can I export all entries under a specific form?
If you don't select any entry from table and select option `Export to csv` and click apply button, you will get a csv file that contains all entries of the form.

= Why I don't have a reply by email option?
`Reply by email` option only available if your form have a email field.

== Changelog ==

= 1.0.0 =
Initial release of plugin.