<?php
namespace CF7DBTOOL;
class Plugin
{
	/**
	 * config class instance
	 * @var object
	 */
	private $config;
	/**
	 * forms class instance
	 * @var object
	 */
	private $forms;
	/**
	 * entries class instance
	 * @var object
	 */
	private $formEntries;
	/**
	 * method __construct()
	 */
	public function __construct()
	{
		/**
		 * call initialize plugin
		 */
		$this->init();
	}

	/**
	 * initialize plugin
	 * @return void
	 */
	public function init()
	{
		require_once 'Config.php';
		require_once 'Forms.php';
		require_once 'FormEntries.php';
		require_once 'ListForms.php';
		require_once 'ListEntries.php';
		require_once 'CsvExport.php';
		require_once 'Mail.php';
		$this->config = new Config();
		add_action('admin_menu', [$this, 'addOptionsPage'], 5);
		add_action('admin_enqueue_scripts', [$this, 'enqueueAdminAssets']);
		add_action('admin_notices', [$this, 'adminNotice']);
		$this->forms = new Forms($this->config);
		$this->formEntries = new FormEntries($this->config);
	}

	/**
	 * add plugin option page in admin menu
	 * @return void
	 */
	public function addOptionsPage()
	{
		add_menu_page(
			'CF7 DB Tool',
			'CF7 DB Tool',
			'manage_options',
			'cf7_dbt',
			[$this, 'optionsPageContent'],
			'dashicons-book',
			50
		);
	}

	/**
	 * callback for option page content
	 * @return void
	 */
	public function optionsPageContent()
	{
		// check is cf7 is exists
		if ( ! class_exists('WPCF7_ContactForm') ) {
			$this->cf7NotFound();
			return;
		}
		// list form entries
		if (isset($_GET['form_id'])) {
			$this->formEntries->allEntries($_GET['form_id']);
			return;
		}
		// show entry details
		if (isset($_GET['entry_id'])) {
			$this->formEntries->renderDetails($_GET['entry_id']);
			return;
		}
		// list forms
		$this->forms->allForms();
	}
	/**
	 * enqueue assets in admin
	 * @return void
	 */
	public function enqueueAdminAssets()
	{
		wp_enqueue_style('cf7-dbt-style', CF7_DBT_URL . '/assets/css/cf7-db-tool.css', '', CF7_DBT_VERSION);
		wp_enqueue_script('cf7-dbt-script', CF7_DBT_URL . '/assets/js/cf7-db-tool.js', ['jquery'], CF7_DBT_VERSION,true);
		wp_localize_script('cf7-dbt-script','cf7DbtObj',[
			'ajaxUrl' => admin_url( 'admin-ajax.php'),
			'nonce' => wp_create_nonce('cf7-dbt-reply-nonce')
		]);
	}
	/**
	 * notice if cf7 is not available
	 */
	public function cf7NotFound()
	{
		ob_start();
		?>
			<div class="wrap">
				<h2><?php _e('CF7 DB Tool - Warning','cf7-db-tool')?></h2>
				<div class="cf7-dbt-warning">
					<h4 style="margin: 10px 0 0"><?php _e('This plugin required CONTACT FORM 7 Plugin','cf7-db-tool')?></h4>
					<p><?php _e('Please install & activate ','cf7-db-tool')?><a href="https://wordpress.org/plugins/contact-form-7/" target="_blank"><?php _e('contact form 7','cf7-db-tool')?></a> <?php _e(' plugin','cf7-db-tool')?>.</p>
				</div>
			</div>
		<?php
		return ob_get_flush();
	}
	/**
	 * admin notice on plugin activation
	 */
	public function adminNotice()
	{
		ob_start();
		if(get_transient('cf7-dbt-warning')):
			?>
			<div class="notice notice-warning is-dismissible">
				<h4 style="margin: 10px 0 0"><?php _e('This plugin required CONTACT FORM 7 Plugin','cf7-db-tool')?></h4>
				<p><?php _e('Please install & activate ','cf7-db-tool')?><a href="https://wordpress.org/plugins/contact-form-7/" target="_blank"><?php _e('contact form 7','cf7-db-tool')?></a> <?php _e(' plugin','cf7-db-tool')?>.</p>
			</div>
			<?php
		endif;
		return ob_get_flush();
	}
}